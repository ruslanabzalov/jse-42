package tsc.abzalov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    @NotNull
    private final String roleName;

    Role(@NotNull final String roleName) {
        this.roleName = roleName;
    }

    @NotNull
    public String getRoleName() {
        return roleName;
    }

}
