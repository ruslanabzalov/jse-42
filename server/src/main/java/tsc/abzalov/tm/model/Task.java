package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "task")
@EqualsAndHashCode(callSuper = true)
public final class Task extends AbstractBusinessEntity {

    @Nullable
    @ManyToOne
    private Project project;
}
