package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "session")
@EqualsAndHashCode(callSuper = true)
public final class Session extends AbstractEntity {

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(name = "open_date")
    private LocalDateTime openDate;

    @Column
    @Nullable
    private String signature;
}
