package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Status.TODO;
import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;
import static tsc.abzalov.tm.util.LiteralConst.*;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractBusinessEntityDTO extends AbstractEntityDTO implements Serializable {

    @Column
    @Nullable
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = TODO;

    @Nullable
    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Nullable
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Nullable
    @Column(name = "user_id")
    private Long userId;

    @NotNull
    @Override
    public String toString() {
        @NotNull val correctName = Optional.ofNullable(name).orElse(DEFAULT_NAME);
        @NotNull val correctDescription = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);
        @NotNull val correctStartDate = (startDate == null)
                ? IS_NOT_STARTED
                : startDate.format(DATE_TIME_FORMATTER);
        @NotNull val correctEndDate = (endDate == null)
                ? IS_NOT_ENDED
                : endDate.format(DATE_TIME_FORMATTER);

        return correctName +
                ": [ID: " + getId() +
                "; Description: " + correctDescription +
                "; Status: " + this.status.getStatusName() +
                "; Start Date: " + correctStartDate +
                "; End Date: " + correctEndDate +
                "; User ID: " + this.userId + "]";
    }

}

