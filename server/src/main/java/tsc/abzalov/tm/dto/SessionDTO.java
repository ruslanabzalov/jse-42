package tsc.abzalov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "session")
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    @Column(name = "user_id")
    private Long userId;

    @Nullable
    @Column(name = "open_date")
    private LocalDateTime openDate;

    @Column
    @Nullable
    private String signature;

    @Nullable
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

}
