package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "project")
@EqualsAndHashCode(callSuper = true)
public class ProjectDTO extends AbstractBusinessEntityDTO implements Serializable {
}
