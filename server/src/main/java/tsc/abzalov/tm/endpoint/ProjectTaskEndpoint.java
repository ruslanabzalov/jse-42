package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.IProjectTaskEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint() {
    }

    public ProjectTaskEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean hasData(@WebParam(name = "session") @Nullable final SessionDTO session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.hasData(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addTaskToProjectById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                     @WebParam(name = "projectId") @Nullable final Long projectId,
                                     @WebParam(name = "taskId") @Nullable final Long taskId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.addTaskToProjectById(taskId, projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public ProjectDTO findTaskProjectById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                          @WebParam(name = "id") @Nullable final Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findProjectById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public TaskDTO findProjectTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                       @WebParam(name = "id") @Nullable final Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findTaskById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findProjectTasksById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                              @WebParam(name = "projectId") @Nullable final Long projectId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findProjectTasksById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteProjectById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                  @WebParam(name = "id") @Nullable final Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.deleteProjectById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteProjectTasksById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                       @WebParam(name = "projectId") @Nullable final Long projectId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.deleteProjectTasksById(session.getUserId(), projectId);
    }

}
