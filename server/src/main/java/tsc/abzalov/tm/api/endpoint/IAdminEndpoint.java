package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.UserDTO;

import java.util.List;

public interface IAdminEndpoint {

    void adminCreateUserWithCustomRole(@Nullable SessionDTO session, @Nullable String login,
                                       @Nullable String password, @Nullable Role role,
                                       @Nullable String firstName, @Nullable String lastName,
                                       @Nullable String email);

    void adminDeleteUserByLogin(@Nullable SessionDTO session, @Nullable String login);

    @Nullable
    UserDTO adminLockUnlockUserById(@Nullable SessionDTO session, @Nullable Long id);

    @Nullable
    UserDTO adminLockUnlockUserByLogin(@Nullable SessionDTO session, @Nullable String login);

    long adminSizeUsers(@Nullable SessionDTO session);

    boolean adminIsEmptyUserList(@Nullable SessionDTO session);

    void adminCreateUserWithEntity(@Nullable SessionDTO session, @Nullable UserDTO user);

    void adminAddAllUsers(@Nullable SessionDTO session, @Nullable List<UserDTO> users);

    @NotNull
    List<UserDTO> adminFindAllUsers(@Nullable SessionDTO session);

    @Nullable
    UserDTO adminFindUsersById(@Nullable SessionDTO session, @Nullable Long id);

    void adminClearAllUsers(@Nullable SessionDTO session);

    void adminRemoveUserById(@Nullable SessionDTO session, @Nullable Long id);

    void adminCreateUser(@Nullable SessionDTO session, @Nullable final String login,
                         @Nullable final String password, @Nullable final String firstName,
                         @Nullable final String lastName, @Nullable final String email);

    boolean adminIsUserExist(@Nullable SessionDTO session, @Nullable String login, @Nullable String email);

    @Nullable
    UserDTO adminFindUserUserById(@Nullable SessionDTO session, @Nullable Long userId);

    @Nullable
    UserDTO adminFindUserByLogin(@Nullable SessionDTO session, @Nullable String login);

    @Nullable
    UserDTO adminEditPasswordById(@Nullable SessionDTO session, @Nullable Long userId, @Nullable String newPassword);

    @Nullable
    UserDTO adminEditUserInfoById(@Nullable SessionDTO session, @Nullable Long userId,
                                  @Nullable String firstName, @Nullable String lastName);

}
