package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;

public interface IFileBackupEndpoint {

    void autoLoadBackup(@Nullable SessionDTO session);

    void autoSaveBackup(@Nullable SessionDTO session);

    void base64LoadBackup(@Nullable SessionDTO session);

    void base64SaveBackup(@Nullable SessionDTO session);

    void binaryLoadBackup(@Nullable SessionDTO session);

    void binarySaveBackup(@Nullable SessionDTO session);

    void fasterXmlJsonLoadBackup(@Nullable SessionDTO session);

    void fasterXmlJsonSaveBackup(@Nullable SessionDTO session);

    void fasterXmlLoadBackup(@Nullable SessionDTO session);

    void fasterXmlSaveBackup(@Nullable SessionDTO session);

    void fasterXmlYamlLoadBackup(@Nullable SessionDTO session);

    void fasterXmlYamlSaveBackup(@Nullable SessionDTO session);

    void jaxbJsonLoadBackup(@Nullable SessionDTO session);

    void jaxbJsonSaveBackup(@Nullable SessionDTO session);

    void jaxbXmlLoadBackup(@Nullable SessionDTO session);

    void jaxbXmlSaveBackup(@Nullable SessionDTO session);

}
