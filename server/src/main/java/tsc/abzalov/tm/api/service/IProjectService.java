package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.dto.ProjectDTO;

public interface IProjectService extends IBusinessEntityService<ProjectDTO> {
}
