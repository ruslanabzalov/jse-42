package tsc.abzalov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    SqlSession getSession();

}
