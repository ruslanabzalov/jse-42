package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO openSession(@Nullable String login, @Nullable String password);

    void closeSession(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session);

    void validateAdminPermissions(@Nullable SessionDTO session, @NotNull IUserService userService);

    @NotNull
    SessionDTO findSession(@Nullable SessionDTO session);

}
