package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.UserDTO;

public interface IUserEndpoint {

    @Nullable
    UserDTO findUserById(@Nullable SessionDTO session);

    @Nullable
    UserDTO editPasswordById(@Nullable SessionDTO session, @Nullable String newPassword);

    @Nullable
    UserDTO editUserInfoById(@Nullable SessionDTO session, @Nullable String firstName, @Nullable String lastName);

}
