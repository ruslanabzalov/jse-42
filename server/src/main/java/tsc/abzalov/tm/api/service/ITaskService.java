package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.dto.TaskDTO;

public interface ITaskService extends IBusinessEntityService<TaskDTO> {
}
