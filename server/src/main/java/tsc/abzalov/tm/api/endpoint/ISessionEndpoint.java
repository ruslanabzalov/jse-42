package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.SessionDTO;

public interface ISessionEndpoint {

    @NotNull
    SessionDTO openSession(@Nullable String login, @Nullable String password);

    void closeSession(@Nullable SessionDTO session);

    @Nullable
    SessionDTO findSession(@Nullable SessionDTO session);

}
