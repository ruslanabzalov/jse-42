package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.dto.TaskDTO;

public final class TaskService extends AbstractBusinessEntityService<TaskDTO> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
