package tsc.abzalov.tm.service;

import lombok.val;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.property.IDatabasePropertyService;
import tsc.abzalov.tm.dto.ProjectDTO;
import tsc.abzalov.tm.dto.SessionDTO;
import tsc.abzalov.tm.dto.TaskDTO;
import tsc.abzalov.tm.dto.UserDTO;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private static final String ENV_ID = "Development";

    @NotNull
    private static final String REPOS_PACKAGE_NAME = "tsc.abzalov.tm.api.repository";

    @NotNull
    private final IDatabasePropertyService propertyService;

    public ConnectionService(@NotNull final IDatabasePropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDialectSetting());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getToDdlSetting());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getShowSqlSetting());

        final StandardServiceRegistry registryBuilder = new StandardServiceRegistryBuilder()
                .applySettings(settings)
                .build();
        final MetadataSources sources = new MetadataSources(registryBuilder);

        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);

        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public SqlSession getSession() {
        @NotNull val url = propertyService.getDatabaseUrl();
        @NotNull val user = propertyService.getDatabaseUser();
        @NotNull val password = propertyService.getDatabasePassword();
        @NotNull val driver = propertyService.getDatabaseDriver();
        @NotNull val datasource = new PooledDataSource(driver, url, user, password);
        @NotNull val transactionFactory = new JdbcTransactionFactory();
        @NotNull val environment = new Environment(ENV_ID, transactionFactory, datasource);

        @NotNull val configuration = new Configuration(environment);
        configuration.addMappers(REPOS_PACKAGE_NAME);
        configuration.setMapUnderscoreToCamelCase(true);
        @NotNull val sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
        return sqlSessionFactory.openSession();
    }

}
