package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Login is empty!");
    }

}
