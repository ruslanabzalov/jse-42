package tsc.abzalov.tm.exception.general;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyServiceLocatorException extends AbstractException {

    public EmptyServiceLocatorException() {
        super("Empty Service Locator!");
    }

}
