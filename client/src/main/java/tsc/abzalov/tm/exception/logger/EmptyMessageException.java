package tsc.abzalov.tm.exception.logger;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyMessageException extends AbstractException {

    public EmptyMessageException() {
        super("Message is empty!");
    }

}
