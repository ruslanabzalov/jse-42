package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;


public final class TaskUpdateByNameCommand extends AbstractCommand {

    public TaskUpdateByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "update-task-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("EDIT TASK BY NAME");
        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areTasksExist = taskEndpoint.tasksSize(session) != 0;
        if (areTasksExist) {
            @NotNull val taskName = inputName();
            @NotNull val taskDescription = inputDescription();
            System.out.println();

            @Nullable val task =
                    taskEndpoint.editTaskByName(session, taskName, taskDescription);
            val wasTaskEdited = Optional.ofNullable(task).isPresent();
            if (wasTaskEdited) {
                System.out.println("Task was successfully updated.\n");
                return;
            }

            System.out.println("Task was not updated! Please, check that task exists and try again.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
